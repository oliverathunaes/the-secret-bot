# secret-bot.py
import os
import asyncio
import time

import random

import discord
from dotenv import load_dotenv

from discord.ext import commands

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
GUILD = os.getenv('DISCORD_GUILD')

bot = commands.Bot(command_prefix='!')


@bot.event
async def on_ready():
    guild = discord.utils.get(bot.guilds, name=GUILD)
    print(f'{bot.user.name} has connected to the following guild(s):')
    print(f'{guild.name} (id: {guild.id})')


@bot.command(name='roll', help="Simulates rolling dice.")
async def roll(ctx, number_of_dice : int, number_of_sides : int):
    dice = [
            str(random.choice(range(1, number_of_sides + 1)))
            for _ in range(number_of_dice)
            ]
    await ctx.send(', '.join(dice))


@bot.command(name='nuke', help='Deletes all messages in current channel.')
@commands.has_role('mod')
async def nuke(ctx):
    def check(m):
        return m.content.lower() == "yes" and m.channel == ctx.channel
    await ctx.send("Are you sure?\nType yes, or wait 10 seconds to cancel.")
    try:
        confirm = await bot.wait_for('message', timeout=10.0, check=check)
    except asyncio.TimeoutError:
        await ctx.send("Took too long to confirm. Cancelling.")
    else:
        await ctx.send("Deleting all messages in this channel.")
        time.sleep(5.0)
        async for i in ctx.channel.history(limit=None):
            await i.delete()


@bot.command(name='fuckme', help='Deletes all messages by the command author in the current channel.')
async def nuke(ctx, confirm="gibberish"):
    def check(m):
        return m.content.lower() == "yes" and m.channel == ctx.channel
    await ctx.send("Are you sure?\nType yes, or wait 10 seconds to cancel.")
    try:
        confirm = await bot.wait_for('message', timeout=10.0, check=check)
    except asyncio.TimeoutError:
        await ctx.send("Took too long to confirm. Cancelling.")
    else:
        await ctx.send("Alrightly. Deleting all your messages in this channel.")
        time.sleep(5.0)
        async for i in ctx.channel.history(limit=None):
            if i.author == ctx.author:
                await i.delete()


@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.errors.CheckFailure):
        await ctx.send('You do not have the correct role for this command.')

bot.run(TOKEN)
